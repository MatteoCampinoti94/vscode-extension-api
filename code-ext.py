#!/usr/bin/python

import bs4
import glob
import json
import os
import requests
import shutil
import sys


def download(author, extension, version):
    url = "https://marketplace.visualstudio.com/_apis/public/"
    url += "gallery/publishers/{}/vsextensions/{}/{}/vspackage".format(
        author,
        extension,
        version
    )

    stream = requests.get(
        url=url,
        stream=True
    )

    package = f"{author}-{extension}-{version}.vsix"

    with open(package, "wb") as ext:
        for chunk in stream.iter_content(chunk_size=1024):
            ext.write(chunk)

    return package


def setup():
    cmd_code = "code"
    platform = ""
    mode = str()
    ext_fldr = str()
    ext_list = []

    if sys.platform == "win32":
        platform = "win32"
        ext_fldr = "%USERPROFILE%\\.vscode\\extensions"
    else:
        platform = "unix"
        ext_fldr = "~/.vscode/extensions"

    argv = sys.argv[1:]

    i = 0
    while i < len(argv) and not mode:
        arg = argv[i]

        if arg in ("-f", "--folder") and i+1 < len(argv):
            ext_fldr = argv[i+1]
            i += 1
        elif arg in ("-c", "--code") and i+1 < len(argv):
            cmd_code = argv[i+1]
            i += 1
        elif arg == "install":
            mode = 'i'
        elif arg == "uninstall":
            mode = 'u'

        i += 1

    ext_list = argv[i:]
    ext_fldr = os.path.expanduser(ext_fldr)

    return {
        "cmd": cmd_code,
        "mode": mode,
        "fldr": ext_fldr,
        "list": ext_list,
        "os": platform
    }


def install(code):
    for ext in code["list"]:
        package = ''

        print(f"Installing {ext} ", end='', flush=True)

        try:
            if '.' not in ext:
                raise

            ext = requests.get(
                f"https://marketplace.visualstudio.com/items?itemName={ext}"
                )

            if ext.status_code != 200:
                raise

            ext = bs4.BeautifulSoup(ext.text, "lxml")

            metadata = ext.find("script", {"class": "vss-extension"})
            metadata = json.loads(metadata.string)

            version = metadata["versions"][0]["version"]

            print(f"{version} ", end='', flush=True)

            package = download(
                metadata["publisher"]["publisherName"],
                metadata["extensionName"],
                version
            )

            return_code = os.system(
                "{} --install-extension {} {}".format(
                    code["cmd"],
                    package,
                    "&>/dev/null" if code["os"] == "unix" else ''
                )
            )

            if return_code != 0:
                print(f"[{return_code}]", end=' ')
                raise

            print('✓')
        except KeyboardInterrupt:
            print('✗')
            return
        except:
            print('✗')
        finally:
            if package:
                os.remove(package)


def uninstall(code):
    for ext in code["list"]:

        try:
            print(f"Uninstalling {ext} ", end='', flush=True)

            ext = glob.glob(f"{ext}-*")

            if not ext:
                print('✗')
            else:
                for e in ext:
                    shutil.rmtree(e)
                print('✓')
        except KeyboardInterrupt:
            print('✗')
            return
        except:
            print('✗')



if __name__ == "__main__":
    code = setup()

    os.chdir(code["fldr"])

    if code["mode"] == 'i':
        install(code)
    elif code["mode"] == 'u':
        uninstall(code)
